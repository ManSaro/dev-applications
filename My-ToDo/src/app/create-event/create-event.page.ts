import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.page.html',
  styleUrls: ['./create-event.page.scss'],
})
export class CreateEventPage {
  title: any;
  description: any;
  startTime: any;
  endTime: any;
  todoDate: any;
  constructor(
    public modalController: ModalController,
    public alertController: AlertController
  ) { }

  back() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  async submit() {
    let myTodoList = [];
    myTodoList = localStorage.myTodoList ? JSON.parse(localStorage.myTodoList) : [];
    let todo = {
      "title": this.title,
      "desc": this.description,
      "todoDate": moment(this.todoDate).format('DD-MM-YYYY'),
      "date": moment(this.startTime).format('DD-MM-YYYY'),
      "timeStamp": Date.parse(this.startTime),
      "sTime": moment(this.startTime).format('hh:mm A'),
      "eTime": moment(this.endTime).format('hh:mm A')
    }
    myTodoList.push(todo);
    localStorage.myTodoList = JSON.stringify(myTodoList);
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Success!',
      message: 'Todo list added successfully',
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.modalController.dismiss({
              'dismissed': true
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
