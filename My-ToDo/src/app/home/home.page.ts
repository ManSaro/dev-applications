import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CreateEventPage } from '../create-event/create-event.page';
import * as _ from 'underscore';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  date: any;
  todayDate: any;
  viewCalendar: boolean = true;
  myTodoList: any = [];
  nextTodo: any = [];
  today: string;
  months: any = [
    { month: "January", expanded: false, value: "01" },
    { month: "February", expanded: false, value: "02" },
    { month: "March", expanded: false, value: "03" },
    { month: "April", expanded: false, value: "04" },
    { month: "May", expanded: false, value: "05" },
    { month: "June", expanded: false, value: "06" },
    { month: "July", expanded: false, value: "07" },
    { month: "August", expanded: false, value: "08" },
    { month: "September", expanded: false, value: "09" },
    { month: "October", expanded: false, value: "10" },
    { month: "November", expanded: false, value: "11" },
    { month: "December", expanded: false, value: "12" }
  ]
  daysInMonth: number;
  all_days: any[];
  filteredTodoList: any;
  viewMonths: boolean = false;
  selectedDate: any;
  showAlert: boolean;
  constructor(
    public modalController: ModalController,
    public loadingController: LoadingController
  ) { }

  async ngOnInit() {
    let todoList = [];
    this.nextTodo = [];
    var d = new Date().toLocaleDateString();
    this.todayDate = d.substr(0, d.indexOf('/'));
    this.today = d.replace(/([/])/g, '-');
    let currentTime = new Date().valueOf();
    let myTodoList = localStorage.myTodoList ? JSON.parse(localStorage.myTodoList) : [];
    if (myTodoList.length > 0) {
      this.showAlert = false;
      await _.each(myTodoList, (todo) => {
        if (todo.todoDate == this.today && todo.timeStamp >= currentTime) {
          todoList.push(todo);
          this.nextTodo.push(todo);
        }
      });
      this.myTodoList = _.uniq(todoList);
      this.myTodoList = _.sortBy(this.myTodoList, 'timeStamp');
      this.nextTodo = _.sortBy(this.nextTodo, 'timeStamp');
    }
    if(this.myTodoList.length){
      this.showAlert = false;
    }
    else{
      this.showAlert = true;
    }
  }
  openCal() {
    this.viewCalendar = true;
    this.viewMonths = false;
  }
  openList() {
    this.viewMonths = true;
    this.viewCalendar = false;
  }
  async onChange($event) {
    let todoList = [];
    this.nextTodo = [];
    this.myTodoList = localStorage.myTodoList ? JSON.parse(localStorage.myTodoList) : [];
    let selectedDate = $event.format('DD-MM-YYYY');
    let currentTime = new Date().valueOf();
    this.todayDate = selectedDate.substr(0, selectedDate.indexOf('-'));
    if (this.myTodoList.length > 0) {
      this.showAlert = false;
      await _.each(this.myTodoList, (todo) => {
        if (todo.todoDate == this.today && todo.todoDate == selectedDate && todo.timeStamp >= currentTime) {
          todoList.push(todo);
          if (todo.timeStamp >= currentTime) {
            this.nextTodo.push(todo);
          }
        }
        else if (todo.todoDate != this.today && todo.todoDate == selectedDate) {
          todoList.push(todo);
          this.nextTodo.push(todo);
        }
      });
      this.myTodoList = todoList;
      this.myTodoList = _.uniq(this.myTodoList);
      this.myTodoList = _.sortBy(this.myTodoList, 'timeStamp');
      this.nextTodo = _.sortBy(this.nextTodo, 'timeStamp');
    }
    if(this.myTodoList.length){
      this.showAlert = false;
    }
    else{
      this.showAlert = true;
    }
  }
  async createEvent() {
    const modal = await this.modalController.create({
      component: CreateEventPage,
      cssClass: 'my-custom-class'
    });
    modal.onDidDismiss().then(async (modelData) => {
      if (modelData !== null) {
        this.myTodoList = localStorage.myTodoList ? JSON.parse(localStorage.myTodoList) : [];
        let todoList = [];
        this.nextTodo = [];
        var d = new Date().toLocaleDateString();
        this.todayDate = d.substr(0, d.indexOf('/'));
        let today = d.replace(/([/])/g, '-');
        let currentTime = new Date().valueOf();
        if (this.myTodoList.length > 0) {
          this.showAlert = false;
          this.presentLoading();
          await _.each(this.myTodoList, (todo) => {
            if (todo.todoDate == today && todo.timeStamp >= currentTime) {
              todoList.push(todo);
            }
            if (todo.timeStamp >= currentTime) {
              this.nextTodo.push(todo);
            }
          });
          this.myTodoList = _.uniq(todoList);
          this.myTodoList = _.sortBy(this.myTodoList, 'timeStamp');
          this.nextTodo = _.sortBy(this.nextTodo, 'timeStamp');
        }
      }
    });
    return await modal.present();
  }
  expandItem(item): void {
    let year = new Date().getFullYear();
    this.daysInMonth = new Date(year, item.value, 0).getDate();
    var all_days = [];
    let counter = 0;
    while (counter != this.daysInMonth) {
      var d = counter + 1;
      all_days.push(d);
      counter++;
    }
    this.all_days = all_days;
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.months.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }
  async filterTodo(date: string, month: string) {
    this.selectedDate = date;
    this.filteredTodoList = [];
    let year = new Date().getFullYear();
    if ((date).toString().length == 1) {
      date = '0' + date;
    }
    var selectedDate = date + '-' + month + '-' + year;
    let todoList = [];
    this.filteredTodoList = localStorage.myTodoList ? JSON.parse(localStorage.myTodoList) : [];
    if (this.filteredTodoList.length > 0) {
      await _.each(this.filteredTodoList, (todo) => {
        if (todo.todoDate == selectedDate) {
          todoList.push(todo);
        }
      });
      this.filteredTodoList = todoList;
      this.filteredTodoList = _.uniq(this.filteredTodoList);
      this.filteredTodoList = _.sortBy(this.filteredTodoList, 'timeStamp');
    }
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 1000,
      cssClass: 'loading-spinner'
    });
    await loading.present();
  }
}
