import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(
    public _http: HttpClient
  ) { }
  /**
   * To handle - Post call to the API to get the response 
   * @param url - API to get response
   * @param obj - Post data
   * @returns - Response data from API
   * @function postCall
   */
  async postCall(url: any, obj: any) {
    return new Promise(async (resolve, reject) => {
      try {
        this._http.post(url, obj)
          .subscribe((data: any) => {
            resolve(data.body);
          }, (e) => {
            resolve([]);
            console.log(e);
          });
      }
      catch (e) {
        console.log(e);
      }
    });
  }
  async getCall(url: any) {
    return new Promise(async (resolve, reject) => {
      try {
        this._http.get(url)
          .subscribe((data: any) => {
            resolve(data);
          }, (e) => {
            resolve([]);
            console.log(e);
          });
      }
      catch (e) {
        console.log(e);
      }
    });
  }
}
