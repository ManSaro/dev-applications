import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { GlobalService } from '../../service/global.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export interface movieModule {
  first: string;
  email: number;
  UserStatus: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  form: FormGroup;
  movieData: any;
  movie: string = 'Avengers';
  searchText: any;
  movieDataList: any;
  displayedColumns: string[] = ['Poster', 'Title', 'Type', 'Year', 'view'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  movieDataSource: MatTableDataSource<movieModule>;
  constructor(
    public g: GlobalService,
    public spinner: NgxSpinnerService,
    public router: Router,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      movie: ['', Validators.compose([Validators.required])]
    });
  }

  async ngOnInit(): Promise<void> {
    console.log(this.movieData);
    await this.listMovie();
  }
  applySearchFilter(filterValue: string) {
    this.movieDataSource.filter = filterValue.trim().toLowerCase();
    if (this.movieDataSource.paginator) {
      this.movieDataSource.paginator.firstPage();
    }
  }
  async listMovie() {
    this.spinner.show();
    let url = "https://www.omdbapi.com/?apikey=1f7328a2&" + "s=" + this.movie + "&plot=full";
    await this.g.getCall(url)
      .then(async (data: any) => {
        console.log(data);
        this.spinner.hide();
        if (data.Response == "True") {
          this.movieDataList = data.Search;
          const ELEMENT_DATA: movieModule[] = this.movieDataList.sort((a: { Title: string; }, b: { Title: any; }) => a.Title.localeCompare(b.Title));
          this.movieDataSource = new MatTableDataSource<movieModule>(ELEMENT_DATA);
          this.movieDataSource.sort = this.sort;
          this.movieDataSource.paginator = this.paginator;
        }
        else {
          this.movieData = '';
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 2000
          });
          Toast.fire({
            icon: "error",
            title: data.Error
          });
        }
      })
  }
  async viewDetails(movie) {
    this.spinner.show();
    let url = "https://www.omdbapi.com/?apikey=1f7328a2&" + "t=" + movie.Title + "&y=" + movie.Year + "&plot=full";
    await this.g.getCall(url)
      .then(async (data: any) => {
        console.log(data);
        this.spinner.hide();
        if (data.Response == "True") {
          this.movieData = data;
        }
        else {
          this.movieData = '';
          const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 2000
          });
          Toast.fire({
            icon: "error",
            title: data.Error
          });
        }
      })
  }
  backTo(event: boolean) {
    if (event == true) {
      this.movieData = '';
    }
  }
  viewList(movie){
    localStorage.selectedMovie = JSON.stringify(movie);
    this.router.navigateByUrl('/detail');
  }
}
