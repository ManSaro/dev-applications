import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { GlobalService } from '../../service/global.service';

interface Provider {
  Poster: string,
  country: string,
  locale: string,
  company: string
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})

export class DetailComponent implements OnInit {
  @Input() data: any;
  @Output() backTo = new EventEmitter<boolean>();
  localData: boolean = false;
  constructor(
    public g: GlobalService,
    public spinner: NgxSpinnerService,
    public router: Router
  ) {
    console.log(this.data);
  }

  async ngOnInit(): Promise<void> {
    if (localStorage.selectedMovie) {
      this.localData = true;
      let movie = JSON.parse(localStorage.selectedMovie);
      this.spinner.show();
      let url = "https://www.omdbapi.com/?apikey=1f7328a2&" + "t=" + movie.Title + "&y=" + movie.Year + "&plot=full";
      await this.g.getCall(url)
        .then(async (data: any) => {
          console.log(data);
          this.spinner.hide();
          if (data.Response == "True") {
            this.data = data;
          }
          else {
            this.data = '';
            const Toast = Swal.mixin({
              toast: true,
              showConfirmButton: false,
              timer: 2000
            });
            Toast.fire({
              icon: "error",
              title: data.Error
            });
          }
        })
    }
  }
  backToHome() {
    this.backTo.emit(true);
  }
  back(){
    this.spinner.show();
    localStorage.removeItem('selectedMovie');
    this.router.navigateByUrl('/home');
  }
}
